/**********************************************************************
    Instituto Tecnológico de Costa Rica
    Estructuras de Datos IC-2001
    II Semestre 2019
    Profesora: Samanta Ramijan Carmiol
    Ejemplos Prácticos: Lista de Estudiantes
    Estudiante: Kevin Zumbado Cruz  2019258634
**********************************************************************/
//Definición de Macros
#define LONGITUD_MAXIMA_NOMBRE 50 
#define LONGITUD_MAXIMA_CARNET 12

//Definición de Estructuras
typedef struct nodo_estudiante
{
	int carnet;
	char *nombre;

	struct nodo_estudiante *ref_siguiente;
}nodo_estudiante;

typedef struct lista_estudiantes
{
	nodo_estudiante *ref_inicio;
	nodo_estudiante *ref_final;//Se agrega para facilitar la implementacion de lista circular.
	int cantidad;
}lista_estudiantes;

//Definición de Funciones
/*-----------------------------------------------------------------------
	crear_nodo
	Entradas: No recibe parámetros
	Salidas: Retorna un puntero de tipo nodo_estudiante al nodo creado
	Funcionamiento: 
		- Solicita al usuario ingresar Nombre y Carnet.
		- Crea un puntero de tipo nodo_estudiante
		- Le asigna al nodo el nombre y carnet ingresados.
		- El nodo apunta a NULL.
		- retorna el puntero al nuevo nodo.
-----------------------------------------------------------------------*/
nodo_estudiante* crear_nodo();
/*-----------------------------------------------------------------------
	inicializar_lista
	Entradas: No recibe parámetros
	Salidas: No retorna datos
	Funcionamiento: 
		- Reserva el espacio para la lista
		- Inicializa la lista de nodos (cantidad e inicio)
-----------------------------------------------------------------------*/
void inicializar_lista();
/*-----------------------------------------------------------------------
	insertar_inicio
	Entradas: Recibe un puntero al nodo nuevo de tipo nodo_estudiante 
	Salidas: No retorna datos
	Funcionamiento:
		- Llama a inicializar lista si la lista está vacia 
		- Enlaza el nuevo nodo al inicio de la lista
		- El nodo nuevo apunta al inicio de la lista
		- Reasigna el inicio de la lista (nuevo nodo)
		- Aumenta la cantidad
-----------------------------------------------------------------------*/
void insertar_inicio(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	insertar_final
	Entradas: Recibe un puntero al nodo nuevo de tipo nodo_estudiante 
	Salidas: No retorna datos
	Funcionamiento: 
		- Llama a inicializar lista si la lista está vacia
		- Si cantidad es cero, llama a insertar_inicio
		- Crea un nodo temporal y le asigna el inicio de la lista
		- Reserva el espacio en memoria para el nodo temporal
		- Recorre la lista e inserta al final
		- Nuevo nodo apunta a NULL
		- Aumenta la cantidad
-----------------------------------------------------------------------*/
void insertar_final(nodo_estudiante* nuevo);
/*-----------------------------------------------------------------------
	borrar_por_indice
	Entradas: Recibe un entero (indice)
	Salidas: No retorna datos
	Funcionamiento:
		- Crea un nodo temporal NULL
		- Reserva el espacio en memoria para el nodo temporal
		- Valida si la lista esta vacia y el indice
		- Encuentra el nodo solicitado
		- Reasigna el nodo inicio (si es el caso)
		- Nodo siguiente puede ser NULL o el siguiente nodo si es que existe alguno
		- Resta a cantidad
-----------------------------------------------------------------------*/
void borrar_por_indice(int indice);
 /*-----------------------------------------------------------------------
	buscar_por_indice
	Entradas: Recibe un entero (indice)
	Salidas: Retorna el nodo temporal de tipo nodo_estudiante
	Funcionamiento: 
		- Crea un nodo de tipo nodo_estudiante temporal NULL
		- Valida si la lista esta vacia y si el indice es valido
		- Asigna al nodo temporal el inicio de la lista
		- Encuentra el nodo solicitado en la posicion indicada y lo retorna
-----------------------------------------------------------------------*/
nodo_estudiante* buscar_por_indice(int indice);

 /*-----------------------------------------------------------------------
	validar_carnets
	Entradas: Recibe dos enteros (carnet_alamcenado, carnet_ingresado)
	Salidas: No retorna datos
	Funcionamiento: 
		- Comprueba si los carnets coinciden 
		- Imprime un mensaje de acuerdo a si coinciden o no
-----------------------------------------------------------------------*/
void validar_carnets(int carnet_almacenado, int carnet_ingresado);
 /*-----------------------------------------------------------------------
	menu
	Entradas: No recibe parámetros
	Salidas: No retorna datos
	Funcionamiento: 
		- Crea un puntero a un nodo temporal de tipo nodo_estudiante
		- Despliega un menu con las opciones disponibles
		- Solicita al usuario una respuesta
		- Ejecuta el caso que corresponda con el deseo del usuario
		- Determina el final de la ejecucion del programa
-----------------------------------------------------------------------*/
void menu();
 /*-----------------------------------------------------------------------
	main
	Entradas: No recibe parámetros
	Salidas: Retorna cero
	Funcionamiento: 
		- Realiza una llamada al metodo menu()
-----------------------------------------------------------------------*/
int main();
 /*-----------------------------------------------------------------------
	get_user_input
	Entradas: Recibe un size_t (max_size)
	Salidas: Retorna un puntero de caracteres (buffer)
	Funcionamiento: 
		- Crea un puntero de caracteres (buffer)
		- Crea un size_t (Characters)
		- Reserva espacio para el buffer
		- Valida si se pudo reservar el espacio del buffer
		- Mediante Characters ingresa los datos al buffer (su direccion de memoria)
		- Forza a poner un fin al array de caracteres '/0'
-----------------------------------------------------------------------*/
char* get_user_input(size_t max_size);
 /*-----------------------------------------------------------------------
	get_user_numerical_input
	Entradas: Recibe un size_t (max_size)
	Salidas: Retorna un entero
	Funcionamiento: 
		- Crea una variable entera que guardara los datos ingresados por el usuario
		- Lo castea mediante la funcion atoi()
		- Dentro de la funcion de atoi realiza una llamada a la funcion get_user_input()
-----------------------------------------------------------------------*/
int get_user_numerical_input(size_t max_size);
