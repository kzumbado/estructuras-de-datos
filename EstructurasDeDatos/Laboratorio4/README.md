Se espera que usted lea y comprenda el código ejemplo, para comprobar lo anterior, debe:
    
    1. Ejecutar el código, realizando las distintas funciones disponibles con datos de
    prueba. Evidenciar su ejecución con al menos 10 capturas de pantalla,
    documentadas en el blog del curso.
    
    2. Abrir ambos archivos en el editor de su preferencia y documentar las declaraciones
    de funciones en el archivo de encabezado, a partir de la implementación disponible
    en el archivo de código fuente. (repositorio).
    
    3. Extender el código para que la lista no sea simple, sino circular..
    

Aspectos Administrativos

1. Límite para la entrega de la asignación: Lunes 2 de septiembre a las 8am.

2. Plataforma de revisión: Repositorio de código git y blog del curso.

3. Cada archivo debe estar debidamente documentado con la información personal del
estudiante que lo escriba, además de explicar su código e indicar cualquier
referencia a código de terceros.

4. Se debe incluir un archivo README que contenga el enunciado de los ejercicios.