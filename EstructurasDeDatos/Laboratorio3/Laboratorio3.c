/*
Estudiante: Kevin Zumbado Cruz
Carnet: 2019258634
Estructuras de Datos
*/

//Bibliotecas necesarias
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


struct estudiante{
	/*
	Estructura de Estudiante
	Atributos:	
	Nombre completo
	Carnet	
	*/
	char completo[100];	
        int carne;
};

struct nodo_estudiante{
	/*
	Estructura de tipo nodo de la estructura estudiante
	*/
	struct estudiante aEstudiante;
	struct estudiante *nodo_sigte;
};

struct lista_estudiante{
	/*
	Estructura de que hace funcion de lista que conecta a los nodos
	*/
	struct nodo_estudiante *cabeza;
};

void imprimir(struct lista_estudiante lista){
	/*
	Funcion que imprime los datos de los estudiantes
	E: una estructura de datos de tipo lista_estudiantes	
	*/
	printf("\n-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-");
	printf("\n\tEstudiante\t\tCarné\n");
	while (lista.cabeza!=NULL){
		printf("\n\t%s \t\t%d \n",lista.cabeza->aEstudiante.completo, lista.cabeza->aEstudiante.carne);
		lista.cabeza=lista.cabeza->nodo_sigte;
	}
	printf("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n");
}

struct lista_estudiante llenar_inicio(struct lista_estudiante lista){
	/*
	Funcion que solicita al usuario los datos de los estudiantes y lo ingresa al principio
	E: una estructura de datos de tipo lista_estudiantes	
	*/
	int cantidad=0;	
	
	printf("Digite la cantidad de Estudiantes que desea ingresar\n");
	scanf("%d",&cantidad);
		
	
	for (int i=0; i<cantidad; i++){

	    	struct lista_estudiante alumnos;
		
		alumnos.cabeza=NULL;

		char nombre[50];
		char apellido[50];

		
		alumnos.cabeza=calloc(1,sizeof(struct nodo_estudiante));
		printf("\nEstudiante #%d:\n",i+1);
		printf("\nIngrese el nombre:\n");
		scanf("%s",nombre);

		printf("Ingrese el apellido:\n");
		scanf("%s",apellido);

		printf("Ingrese el carné:\n");
		scanf("%d", &alumnos.cabeza->aEstudiante.carne);

		strcpy(alumnos.cabeza->aEstudiante.completo,nombre);
		strcat(alumnos.cabeza->aEstudiante.completo," ");
		strcat(alumnos.cabeza->aEstudiante.completo,apellido);
		
		alumnos.cabeza->nodo_sigte=NULL;

		if (lista.cabeza==NULL){
			lista.cabeza=alumnos.cabeza;
		}else{
			
			alumnos.cabeza->nodo_sigte= lista.cabeza;
			lista.cabeza=alumnos.cabeza;
		}

	}
	
	return lista;

}

struct lista_estudiante llenar_final(struct lista_estudiante lista){
	/*
	Funcion que solicita al usuario los datos de los estudiantes y los ingresa al final
	E: una estructura de datos de tipo lista_estudiantes	
	*/
	int cantidad=0;	
	
	printf("\nDigite la cantidad de Estudiantes que desea ingresar\n");
	scanf("%d",&cantidad);
		
	
	for (int i=0; i<cantidad; i++){

	    	struct lista_estudiante alumnos;
		
		alumnos.cabeza=NULL;

		char nombre[50];
		char apellido[50];
		
		alumnos.cabeza=calloc(1,sizeof(struct nodo_estudiante));	
		printf("\nEstudiante #%d:\n",i+1);
		printf("\nIngrese el nombre:\n");
		scanf("%s",nombre);

		printf("Ingrese el apellido:\n");
		scanf("%s",apellido);

		printf("Ingrese el carné:\n");
		scanf("%d", &alumnos.cabeza->aEstudiante.carne);

		strcpy(alumnos.cabeza->aEstudiante.completo,nombre);
		strcat(alumnos.cabeza->aEstudiante.completo," ");
		strcat(alumnos.cabeza->aEstudiante.completo,apellido);
		
		alumnos.cabeza->nodo_sigte=NULL;

		if (lista.cabeza==NULL){
			lista.cabeza=alumnos.cabeza;
		}else{
			
			struct lista_estudiante aux;
			aux.cabeza=lista.cabeza;
			
			while (aux.cabeza->nodo_sigte!=NULL){
				aux.cabeza=aux.cabeza->nodo_sigte;
			}
			aux.cabeza->nodo_sigte=alumnos.cabeza;
		
		}

	}
	
	return lista;

}

struct lista_estudiante eliminar_estudiante(struct lista_estudiante lista, int posicion){
	/*
	Funcion que elimina un estudiante en una posicion especifica.
	E: una estructura de datos de tipo lista_estudiantes y un entero con la posicion de dicho estudiante.
	S: una lista modificada.
	R: El index debe ser valido.	
	*/
	
	int total=0;	
	
	struct lista_estudiante aux,tmp,ant,sig;//ant= anterior nodo; sig= siguiente nodo; aux= auxiliar; tmp= temporal;
	aux.cabeza=lista.cabeza;

	while (aux.cabeza!=NULL){
		aux.cabeza=aux.cabeza->nodo_sigte;
		total++;
	}
	
	if (posicion>-1 & posicion<total){
		tmp.cabeza=lista.cabeza;		

		if (posicion==0){
			lista.cabeza=lista.cabeza->nodo_sigte;
			printf("\nSe ha eliminado el estudiante %s\n\n",tmp.cabeza->aEstudiante.completo);
			free(tmp.cabeza);
		}else if(posicion==total-1){

			while (posicion-1!=0){
				tmp.cabeza=tmp.cabeza->nodo_sigte;
				posicion--;
			}
			ant.cabeza=tmp.cabeza;
			tmp.cabeza=tmp.cabeza->nodo_sigte;
			printf("\nSe ha eliminado el estudiante %s\n\n",tmp.cabeza->aEstudiante.completo);
			ant.cabeza->nodo_sigte=NULL;
			free(tmp.cabeza);
			

		}else{
			while(posicion!=1){
				tmp.cabeza=tmp.cabeza->nodo_sigte;
				posicion--;
			}		
			ant.cabeza=tmp.cabeza;
			tmp.cabeza=tmp.cabeza->nodo_sigte;
			ant.cabeza->nodo_sigte=tmp.cabeza->nodo_sigte;
			
			
			printf("\nSe ha eliminado el estudiante %s\n\n",tmp.cabeza->aEstudiante.completo);
		
			free(tmp.cabeza);			
			
		}

		return lista;
	}else{
		printf("\nError: Se ha ingresado un index no valido\n\n");
		return lista;
	}
	
}

int validar_posicion_estudiante(struct lista_estudiante lista){
	/*
	Funcion que valida si el carnet que ingreso el usuario es correcto o no
	E: un array de tipo estudiante
	S: 1 o 0
	R: La posicion debe estar en el rango sino -1	
	*/

	int posicion;
	int indice=0;
	int total=0;

	printf("\nIngrese la posicion que desea validar: ");
	scanf("%d",&posicion);
	
	struct lista_estudiante aux;
	aux.cabeza=lista.cabeza;	
	
	while (aux.cabeza!=NULL){
		aux.cabeza=aux.cabeza->nodo_sigte;
		total++;
	}
	
	if (posicion>-1 & posicion<total){
		
		while (lista.cabeza!=NULL){
			if (indice==posicion){
				
				int respuesta;

				printf("\nIngrese el Carne del Estudiante: %s\n",lista.cabeza->aEstudiante.completo);
				scanf("%d",&respuesta);

				if (respuesta==lista.cabeza->aEstudiante.carne){

					return 1;
				
				}else{

					return 0;	
				}
			}
			
			lista.cabeza=lista.cabeza->nodo_sigte;
			indice++;
		}

	}else{

		return -1;
	}
}
 
int main() {
	/*
	Funcion principal que actua como la funcion nexo de las demas
	controla el flujo del programa
	*/

	 
	struct lista_estudiante lista;
	lista.cabeza=NULL;
	int loop=0;
	int opcion;
	int posicion=0;

	//Menu
	while(loop==0){

		printf("\nBienvenido al sistema de ingreso de Estudiantes\n1.\tIngresar Estudiante al final\n2.\tIngresar Estudiante al inicio\n3.\tAdivinar Carné de Estudiante\n4.\tEliminar un Estudiante\n5.\tImprimir Lista de Estudiantes\n6.\tSalir\n");
		scanf("%d",&opcion);

		switch(opcion){
			case 1:

				lista=llenar_final(lista);
			break;

			case 2:
				lista=llenar_inicio(lista);
			break;
			
			case 3:
				if (lista.cabeza!=NULL){
					int resultado;
					resultado=validar_posicion_estudiante(lista);
				
					if (resultado==1){
					printf("\nCorrecto!!\n");	
					}else if(resultado==0){
					printf("\nIncorrecto :C\n");
					}else{
					printf("Posicion invalida\n");
					}
				}else{
				printf("\nNo hay estudiantes guardados\n");
				}
			break;

			case 4:
				printf("\nIngrese la posicion del estudiante que desea eliminar: ");
				scanf("%d",&posicion);
				lista=eliminar_estudiante(lista, posicion);
			break;			

			case 5:
				if (lista.cabeza!=NULL){
					imprimir(lista);
				}else{
					printf("\nNo hay estudiantes guardados\n");
				}
			break;

			case 6:

				loop=1;
			break;
		}
	}

	return 0;
 
}


