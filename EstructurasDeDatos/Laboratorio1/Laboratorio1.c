/*
Estudiante: Kevin Zumbado Cruz
Carnet: 2019258634
Estructuras de Datos
*/

//Bibliotecas necesarias
#include <stdio.h>
#include <string.h>


struct estudiante{
	/*
	Estructura de Estudiante
	Atributos:	
	Nombre completo
	Carnet	
	*/
	char completo[100];
	int carne;
};

void imprimir(struct estudiante grupo[10]){
	/*
	Funcion que imprime los datos de los estudiantes
	E: un array de tipo estudiante
	*/

	for (int i=0; i<10; i++){
		printf("\nEstudiante #%d: %s\nCarné: %d",i+1,grupo[i].completo,grupo[i].carne);
	}

}

void llenar_estudiante(struct estudiante grupo[10]){
	/*
	Funcion que solicita al usuario los datos de los estudiantes
	E: un array de tipo estudiante	
	*/
	
	for (int i=0; i<10; i++){

		char nombre[50];
		char apellido[50];

		printf("\nEstudiante #%d:\n",i+1);
		printf("\nIngrese el nombre:\n");
		scanf("%s",nombre);

		printf("Ingrese el apellido:\n");
		scanf("%s",apellido);

		printf("Ingrese el carné:\n");
		scanf("%d", &grupo[i].carne);

		strcpy(grupo[i].completo,nombre);
		strcat(grupo[i].completo," ");
		strcat(grupo[i].completo,apellido);

	}

}

int validar_posicion_estudiante(struct estudiante grupo[10]){
/*
Funcion que valida si el carnet que ingreso el usuario es correcto o no
E: un array de tipo estudiante
S: 1 o 0
R: La posicion debe estar en el rango sino -1	
*/

int posicion;

printf("\nQue posicion desea validar?\n");
scanf("%d",&posicion);

if (posicion>-1 & posicion<10){
	int respuesta;

	printf("Ingrese el Carne del Estudiante: %s\n",grupo[posicion].completo);
	scanf("%d",&respuesta);

	if (respuesta==grupo[posicion].carne){

	return 1;
	}else{

	return 0;	
	}
}else{

	return -1;
}
}

int validar_array(struct estudiante grupo[10]){
/*
Funcion que valida si el array de tipo estudiante posee datos o no
E: un array de tipo estudiante
S: 1 o 0	
*/

for (int i=0; i<10;i++){
	if (grupo[i].carne==0){
	return 0;
	break;
	}
}

return 1;
}
 
int main() {
/*
Funcion principal que actua como la funcion nexo de las demas
controla el flujo del programa
*/

 
struct estudiante grupo[10];
int loop=0;
int opcion;

//Menu
while(loop==0){

printf("\nBienvenido al sistema de ingreso de Estudiantes\n1.\tIngresar Estudiantes\n2.\tAdivinar Carné de Estudiante\n3.\tImprimir Lista de Estudiantes\n4.\tSalir\n");
scanf("%d",&opcion);

	switch(opcion){
	case 1:

	llenar_estudiante(grupo);
	break;
	
	case 2:
	if (validar_array(grupo)!=0){
		int resultado;
		resultado=validar_posicion_estudiante(grupo);
	
		if (resultado==1){
		printf("Correcto!!\n");	
		}else if(resultado==0){
		printf("Incorrecto\n");
		}else{
		printf("Posicion invalida\n");
		}
	}else{
	printf("No hay estudiantes guardados\n");
	}
	break;

	case 3:
	if (validar_array(grupo)!=0){
	imprimir(grupo);
	}else{
	printf("No hay estudiantes guardados\n");
	}
	break;

	case 4:

	loop=1;
	break;
	}
}

return 0;
 
}


