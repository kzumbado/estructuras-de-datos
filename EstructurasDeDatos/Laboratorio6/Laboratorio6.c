/*
Estudiante: Kevin Zumbado Cruz
Carnet: 2019258634
Estructuras de Datos
Laboratorio #6
*/

//Bibliotecas necesarias
#include <stdio.h>
#include <stdlib.h>


struct nodo{

	int valor;
	struct nodo *nodo_izq;
	struct nodo *nodo_der;
};

struct nodo *raiz=NULL;

void insertar (int x){
	
	struct nodo *nuevo = malloc(sizeof(struct nodo));
	nuevo->valor=x;
	nuevo->nodo_izq=NULL;
	nuevo->nodo_der=NULL;
	
	if (raiz==NULL){
		raiz=nuevo;	
	}else{
		struct nodo *antr, *reco;
		antr=NULL;
		reco=raiz;
		while (reco!=NULL){
			antr=reco;
			if (x<reco->valor){
				reco=reco->nodo_izq;
			}else{
				reco=reco->nodo_der;		
			}
		}
		if (x<antr->valor){
			antr->nodo_izq=nuevo;
		}else{
			antr->nodo_der=nuevo;		
		}
	}

}

void imprimirPostOrden(struct nodo *reco){

	if (reco!=NULL){
		imprimirPostOrden(reco->nodo_izq);
		imprimirPostOrden(reco->nodo_der);
		printf("%d->",reco->valor);
	}

}

 
int main() {
	/*
	Funcion principal que actua como la funcion nexo de las demas
	controla el flujo del programa
	*/
	int loop=0;
	int opcion;
	
	int n;
	//Menu
	while(loop==0){

		printf("\nBienvenido al sistema de ingreso de Estudiantes\n1.\tIngresar valores\n2.\tImprimir Arbol\n3.\tSalir\n");
		scanf("%d",&opcion);

		switch(opcion){
			case 1:
				printf("\nIngrese un valor numerico: ");
				scanf("%d",&n);
				insertar(n);
				n=0;
			break;
			
			case 2:				
				printf("\n");
				imprimirPostOrden(raiz);
				printf("NULL\n");
			break;

			case 3:

				loop=1;
			break;
		}
	}

	return 0;
 
}


