//Biblioteca necesaria
import java.util.Date;

public class Cuenta{
  //Atributos
  private int id;
  private double balance;
  private double tasaDeInteresAnual;
  private Date fechaDeCreacion = new Date();
  
  //Constructor vacio que ingresa datos default al objeto
  public Cuenta(){
    this.id=0;
    this.balance=0;
    this.tasaDeInteresAnual=0;
  }

  //Constructor que recibe ciertos parametros para la creacion de cuentas
  public Cuenta(int pId, double pBalance){
    this.id=pId;
    this.balance=pBalance;
  }

  //Getters&Setters
  public int getId(){
    return this.id; 
  }
  
  public void setId(int pId){
    this.id=pId;
  }

  public double getBalance(){
    return this.balance; 
  }

  public void setBalance(double pBalance){
    this.balance=pBalance;
  }

  public double getTasaDeInteresAnual(){
    return this.tasaDeInteresAnual; 
  }

  public void setTasaDeInteresAnual(double pTasaDeInteresAnual){
    this.tasaDeInteresAnual=pTasaDeInteresAnual;
  }

  public Date getFecha(){
    return this.fechaDeCreacion;
  }

  //Metodo que retorna la tasa de interes mensual
  public double obtenerTasaDeInteresMensual(){
    return calcularInteresMensual();
  }

  //Metodo que calcula la tasa de interes mensual
  public double calcularInteresMensual(){
    return this.balance * this.tasaDeInteresAnual;
  }

  //Metodo que simula un retiro de dinero y verifica si hay sufuciente en el balance
  public boolean retirarDinero(double pRetiro){
    if (pRetiro<=this.balance){
      this.balance-=pRetiro;
      return true;
    }else{
      return false;
    }
  }

  //Metodo que actualiza el estado de balance
  public void depositarDinero(double pDeposito){
    this.balance+=pDeposito;
  }

}