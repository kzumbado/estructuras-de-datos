/*
Estudiante: Kevin Zumbado Cruz
Carnet: 2019258634
Programacion Orientada a Objetos
*/

/*Bibliotecas necesarias*/
import java.text.SimpleDateFormat;
import java.util.Scanner;

/*Clase Principal */
class Main {

  public static void cajeroAutomatico(){
    /*Metodo que simula un cajero automatico*/

    //Objeto Cajero de la clase ATM
    ATM cajero = new ATM();
    Scanner sc = new Scanner(System.in);

    //Loop infinito para solicitar el id
    while (true){
      System.out.print("\n\nIngrese su id: ");
      int respuesta = sc.nextInt();

      if (cajero.comprobarId(respuesta)){
        cajero.solicitarOpcion(respuesta);
      }
    }
  }

  public static void main(String[] args) {
    /*Metodo principal*/

    /*Ejemplos del uso de onjetos de la clase Cuenta*/
    
    Cuenta cuenta1 = new Cuenta(1122,500000);
    cuenta1.setTasaDeInteresAnual(0.45);
    cuenta1.depositarDinero(150000);
    cuenta1.retirarDinero(200000);

    //Da formato a la fecha para que no salgan datos extras
    SimpleDateFormat formato = new SimpleDateFormat("dd/MMM/yyyy");

    System.out.println("\nBalance: "+ cuenta1.getBalance() + " CRC"+
    "\nTasa de Interes Mensual: "+ cuenta1.obtenerTasaDeInteresMensual() + " CRC"+
    "\nFecha de Creacion: "+ formato.format(cuenta1.getFecha()));

    Cuenta cuenta2 = new Cuenta(2233,600000);
    cuenta2.setTasaDeInteresAnual(0.15);
    cuenta2.depositarDinero(30000);
    cuenta2.retirarDinero(120000);

    System.out.println("\nBalance: "+ cuenta2.getBalance() + " CRC"+
    "\nTasa de Interes Mensual: "+ cuenta2.obtenerTasaDeInteresMensual() + " CRC"+
    "\nFecha de Creacion: "+ formato.format(cuenta2.getFecha()));

    //Llamada al metodo que simula un cajero automatico
    cajeroAutomatico();

  }
}