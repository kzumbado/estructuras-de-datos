//Biblioteca necesaria
import java.util.Scanner;

public class ATM{
  //Atributo
  private Cuenta[] cliente;

  //Scanner para solicitar datos al usuario
  Scanner sc = new Scanner(System.in);

  //Constructor vacio que inicializa el objeto cliente como un array de 10 y crea los clientes
  public ATM(){
    this.cliente=new Cuenta[10];
    crearCliente();
  }

  //Imprime el menu
  public void menu(){
    System.out.println("\nMenu Principal\n\n1. Ver el balance actual\n2. Retirar dinero\n3. Depositar dinero\n4. Salir\n");
  }

  //Crea los clientes por default
  public void crearCliente(){
    for (int i=0; i<10;i++){
      this.cliente[i]= new Cuenta(i,100000);
    }
  }

  //Despliega el menu y solcita opciones de lo que desea realizar al usuario
  public void solicitarOpcion(int id){
    boolean condicion=true;

    while (condicion==true){
      menu();
      int opcion= sc.nextInt();
      double monto=0;

      switch (opcion){
        case 1:
          System.out.println("\nSu balance actual es: "+ cliente[id].getBalance() +" CRC");
        break;

        case 2:
          System.out.print("\nIngrese el monto que desea retirar: ");
          monto= sc.nextDouble();

          if (cliente[id].retirarDinero(monto)){
            System.out.println("\nSe ha retirado con exito el monto de "+monto+" CRC");
          }else{
            System.out.println("\nError: Fondos insuficientes");
          }
        break;

        case 3:
          System.out.print("\nIngrese el monto que desea depositar: ");
          monto= sc.nextDouble();
          cliente[id].depositarDinero(monto);
          System.out.println("\nSe ha depositado con exito el monto de "+ monto +" CRC");
        break;

        case 4:
          condicion=false;
        break;

        default:
          System.out.println("\nError: Opcion no valida");
        break;

      }
    }
  }

  //Comprueba si el id ingresado pertenece a algun usuario
  public boolean comprobarId(int id){
    for (int i=0;i<10;i++){
      if (id==cliente[i].getId()){
        return true;
      }
    }
    return false;
  }
}