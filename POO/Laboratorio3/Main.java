import java.util.Scanner;

class Main {
  public static void main(String[] args) {
    
    Scanner sc = new Scanner(System.in);
    PilaTEC pila = new PilaTEC();

    for(int i=0;i<5;i++){
      System.out.print((i+1)+".\tIngrese un número: ");
      pila.push(sc.nextInt());
    }

    PilaTEC pila2 = pila;
    
    System.out.println("Pila: ");
    
    for(int j=0;j<5;j++){
      System.out.print(" "+pila2.peek());
      pila2.pop();
    }
    sc.close();
  }
}